class CreateApplications < ActiveRecord::Migration[5.1]
  def change
    # Seems like `applications` relation is a join one between `users` and `vacancies` relations.
    # But since there is no `Vacancy` model in the task decided not to add any additional index.
    #
    # Also decided not to add unique index (suppose one recruiter can have many vacancies).
    create_table :applications do |t|
      t.string :status, null: false
      t.integer :recruiter_id, null: false
      t.integer :candidate_id, null: false
      t.timestamps
    end
  end
end
