class CreateApplicationStatusTransitions < ActiveRecord::Migration[5.1]
  def change
    # It's just for analytics purposes. Don't know how (if) it will be used,
    # so decided not to add indexes so as not to hurt the performance on inserts.
    create_table :application_status_transitions do |t|
      t.references :application, foreign_key: true
      t.string :namespace
      t.string :event
      t.string :from
      t.string :to
      t.timestamp :created_at
    end
  end
end
