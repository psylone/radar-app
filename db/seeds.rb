require 'database_cleaner'

DatabaseCleaner.clean_with(:truncation)

candidates = User.candidate.create(
  [
    { name: 'Bob', email: 'bob@example.com', password: 'givemeatoken' },
    { name: 'Alice', email: 'alice@example.com', password: 'givemeatoken' }
  ]
)

recruiter = User.recruiter.create(name: 'Erin', email: 'erin@example.com', password: 'givemeatoken')

Application.create(
  [
    { recruiter: recruiter, candidate: candidates[0], status: 'unread' },
    { recruiter: recruiter, candidate: candidates[1], status: 'unread' }
  ]
)
