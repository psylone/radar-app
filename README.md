# RadarApp API

## Системные требования

+ Ruby v2.4.3+
+ Rails v5.1.4+
+ Bundler v1.16.1+
+ PostgreSQL v9.4.5+

## Конфигурирование и запуск приложения

```
# Клонирование репозитория
git clone https://gitlab.com/psylone/radar-app.git && cd radar-app

# Установка зависимостей
bundle

# Создание файла конфигурации
echo DATABASE_URL=postgres://<username>:<password>@<host>:5432/radar_app > .env

# Создание базы данных
bin/rails db:setup

# Запуск приложения
bin/rails s
```

## Описание приложения

Основные сущности приложения:

+ `User` (пользователь)
+ `Application` (отклик)

Каждый отклик может находиться в одном из следующих состояний:

+ `unread`
+ `read`
+ `rejected`
+ `declined`
+ `improper`
+ `accepted`

Изначально все отклики находятся в статусе `unread`. Порядок переходов между состояниями:

![Alt text](https://monosnap.com/file/kDJLiXzAo3keiAJZmEa27h1CyryuAT.png)

Пользователи имеют атрибут `kind` который может принимать одно из 2-х значений:

+ `candidate`
+ `recruiter`

По умолчанию `kind = 'candidate'`.

## Описание реализации

Приложение использует фреймворк [Rails](http://rubyonrails.org/) в режиме [API](http://guides.rubyonrails.org/api_app.html).

Для управления состояниями откликов используется [конечный автомат](github.com/state-machines/state_machines-activerecord).

Обработка наиболее общих исключений находится в модуле [`ExceptionHandling`](https://gitlab.com/psylone/radar-app/blob/master/app/controllers/concerns/exception_handling.rb). Обработка исключений, специфичных для доменной области осуществляется в эндпоинтах, например: [обработка исключения](https://gitlab.com/psylone/radar-app/blob/master/app/controllers/applications_controller.rb#L10) при попытке перевести отклик в отсутствующий статус.

Обработка исключений предусматривает формирование ответа в формате JSON, например:

```
curl -X POST --url "localhost:3000/applications/1/proceed" -H "Accept: application/json" -d "status=unknown"
```

```json
{
  "error": "incorrect status 'unknown' during the transition from 'read'",
  "status": 422
}
```

Для выолнения задач, специфических для бизнеса, используются _операции_. Каждая операция представлена простым Ruby-классом, реализующим метод `#call`. Например, изменение статуса отклика выполняется с помощью операции `ChangeApplicationStatus`:

```ruby
ChangeApplicationStatus.new(application).call
```

## Эндпоинты

### Изменение статуса отклика

```
POST /applications/:application_id/proceed
```

**Параметры:**

+ `status` (`String`). Статус в который необходимо перевести отклик.

**Пример:**

```
curl -X POST --url "localhost:3000/applications/1/proceed" -H "Accept: application/json" -d "status=read"
```

**Ответ** (код ответа **201**):

```json
{
  "candidate_id": 2,
  "created_at": "2018-02-02T16:06:43.441Z",
  "id": 2,
  "recruiter_id": 3,
  "status": "read",
  "updated_at": "2018-02-02T16:34:50.751Z"
}
```

**Исключения:**

+ Статус отклика недопустим (код ответа **422**):

```json
{
  "error": "incorrect status 'unknown' during the transition from 'read'",
  "status": 422
}
```

+ Отсутствует параметр запроса (код ответа **400**):

```json
{
  "error": "param is missing or the value is empty: status",
  "status": 400
}
```

+ Отсутствует отклик (код ответа **404**):

```json
{
  "error": "Couldn't find Application with 'id'=101",
  "status": 404
}
```

### Получение токена (аутентификация)

```
POST /auth
```

**Параметры:**

+ `email` (`String`)
+ `password` (`String`)

**Пример:**

```
# Работающий пример с данными из `db/seeds.rb`
curl -X POST --url "localhost:3000/auth" -H "Accept: application/json" -d "email=erin@example.com" -d "password=givemeatoken"
```

**Ответ** (код ответа **201**):

```json
{
  "token": "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ.qFuvthW_OEkn_0Wvaag5xIA0YiJUXRYE6DHKuoyxOqE"
}
```

**Исключения:**

+ Аутентификационные данные не являются верными (код ответа **401**):

```json
{
  "error": "invalid authentication credentials",
  "status": 401
}
```

## Запуск тестов

```
bundle exec rspec
```

## Roadmap

- [x] Authorization
- [ ] Serializers (take a look at [fast_jsonapi](https://github.com/Netflix/fast_jsonapi))
- [ ] API versioning (use headers approach)
- [ ] Deploy (docker compose)
