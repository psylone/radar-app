source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

gem 'rails', '~> 5.1.4'
gem 'pg', '~> 0.18'
gem 'puma', '~> 3.7'

# AUTH

gem 'bcrypt', '~> 3.1.7'
gem 'jwt'

# DOMAIN MODEL
gem 'state_machines-activerecord'
gem 'state_machines-audit_trail'

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
# gem 'rack-cors'

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # It's placed here because no needed to load any ENV variable before the
  # `RadarApp::Application` class will be defined in `radar-app/config/application.rb`.
  # See: https://github.com/bkeepers/dotenv#note-on-load-order.
  gem 'dotenv-rails'
  gem 'rspec-rails'
  gem 'pry-rails'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'faker'
  gem 'database_cleaner'
  gem 'factory_bot_rails'
  gem 'shoulda-matchers'
  gem 'state_machines-rspec'
end
