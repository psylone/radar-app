class ApplicationController < ActionController::API

  include ExceptionHandling
  include BearerToken

  attr_reader :current_user

  before_action :authorize_request

  private

  def authorize_request
    @current_user = AuthorizeRequest.new(bearer_token).call
  rescue AuthorizeRequest::InvalidTokenError => error
    json_response_with_error(error, :unauthorized)
  end

end
