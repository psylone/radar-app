class ApplicationsController < ApplicationController

  before_action :find_application, only: [:proceed]
  before_action :authorize_recruiter, only: [:proceed]

  def proceed
    ChangeApplicationStatus.new(@application, proceed_params).call

    render json: @application, status: :created
  # This error is a domain specific, so placed here.
  rescue ChangeApplicationStatus::IncorrectStatusError => error
    json_response_with_error(error, :unprocessable_entity)
  end

  private

  def find_application
    @application ||= Application.find(params[:application_id])
  end

  def authorize_recruiter
    # Let's use a simple authorization flow here. We can add a more
    # sophisticated solution when it's needed (like Pundit or something else).
    raise ActionController::Forbidden unless @application.recruiter == current_user
  end

  def proceed_params
    params.require(:status)
  end

end
