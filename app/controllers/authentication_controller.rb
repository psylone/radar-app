class AuthenticationController < ApplicationController

  skip_before_action :authorize_request, only: [:auth]

  def auth
    token = AuthenticateUser.new(*auth_params).call

    render json: { token: token }, status: :created
  rescue AuthenticateUser::InvalidCredentials => error
    json_response_with_error(error, :unauthorized)
  end

  private

  def auth_params
    params.require([:email, :password])
  end

end
