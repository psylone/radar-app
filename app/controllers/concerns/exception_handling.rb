module ExceptionHandling
  extend ActiveSupport::Concern

  included do
    rescue_from ActionController::ParameterMissing do |error|
      json_response_with_error(error, :bad_request)
    end

    rescue_from ActionController::Forbidden do |error|
      json_response_with_error(error, :forbidden)
    end

    rescue_from ActiveRecord::RecordNotFound do |error|
      json_response_with_error(error, :not_found)
    end
  end

  private

  def json_response_with_error(error, status)
    response = { error: error.message, status: Rack::Utils.status_code(status) }

    render json: response, status: status
  end
end
