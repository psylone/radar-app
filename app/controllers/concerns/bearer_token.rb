module BearerToken

  BEARER_TOKEN_PATTERN = %r{\ABearer (?<token>.+)\z}

  def bearer_token
    token_match = authorization_header&.match(BEARER_TOKEN_PATTERN)

    # String#match returns nil if it doesn't match the pattern:
    # http://ruby-doc.org/core-2.5.0/String.html#method-i-match
    token_match ? token_match[:token] : nil
  end

  private

  def authorization_header
    request.headers['Authorization']
  end

end
