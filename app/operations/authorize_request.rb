class AuthorizeRequest

  class InvalidTokenError < StandardError; end

  def initialize(token)
    @token = token
  end

  def call
    payload = JWTService.decode(@token)

    User.find(payload['user_id'])
  rescue JWT::DecodeError => error
    # We re-raise an exception because this is the different layer of abstraction.
    # Of course, we can leave it and catch JWT::DecodeError on the upper level, but
    # it will violate the separation of concerns (abstractions) principle.
    raise InvalidTokenError, 'invalid authentication token'
  end

end
