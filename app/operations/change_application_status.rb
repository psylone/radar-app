class ChangeApplicationStatus

  class IncorrectStatusError < StandardError; end

  def initialize(application, desired_status)
    @application = application
    @desired_status = desired_status
  end

  def call
    if status_transition = find_status_transition
      # See details: http://www.rubydoc.info/gems/state_machines/StateMachines/Transition#perform-instance_method
      status_transition.perform
    else
      raise IncorrectStatusError, "incorrect status '#{@desired_status}' during the transition from '#{@application.status}'"
    end
  end

  private

  def find_status_transition
    # We can use the same names for events as for statuses. In this case this logic
    # will be a little bit different (maybe easier), but as for me imperative manner
    # sounds more natural.
    @application.status_transitions.find do |transition|
      transition.to == @desired_status
    end
  end

end
