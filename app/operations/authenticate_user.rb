class AuthenticateUser

  class InvalidCredentials < StandardError; end

  def initialize(email, password)
    @email = email
    @password = password
  end

  def call
    if user = User.find_by(email: @email)&.authenticate(@password)
      JWTService.encode(user_id: user.id)
    else
      raise InvalidCredentials, 'invalid authentication credentials'
    end
  end

end
