class Application < ApplicationRecord
  include Statuses

  validates :status, presence: true

  belongs_to :recruiter, class_name: 'User'
  belongs_to :candidate, class_name: 'User'
end
