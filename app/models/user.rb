class User < ApplicationRecord
  KINDS = %w[candidate recruiter].freeze

  has_secure_password

  validates :name, presence: true
  validates :email, presence: true, uniqueness: true
  validates :password_digest, presence: true
  validates :kind, presence: true, inclusion: { in: KINDS }

  # Submitted applications for the candidate.
  has_many :submitted_applications, class_name: 'Application', foreign_key: :candidate_id, dependent: :destroy
  # Responded applications from the candidates for the recruiter.
  has_many :responded_applications, class_name: 'Application', foreign_key: :recruiter_id, dependent: :destroy

  KINDS.each do |kind|
    scope kind, -> { where(kind: kind) }
  end
end
