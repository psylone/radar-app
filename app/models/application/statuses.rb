class Application
  module Statuses
    extend ActiveSupport::Concern

    included do
      state_machine :status, initial: :unread do
        audit_trail

        event :read do
          transition unread: :read
        end

        event :reject do
          transition unread: :rejected
        end

        event :decline do
          transition read: :declined
        end

        event :improper do
          transition read: :improper
        end

        event :accept do
          transition read: :accepted
        end
      end
    end
  end
end
