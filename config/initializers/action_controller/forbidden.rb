module ActionController
  class Forbidden < ActionControllerError
    def message
      'forbidden'
    end
  end
end
