Rails.application.routes.draw do

  post :auth, to: 'authentication#auth'

  resources :applications, only: [], param: :application_id do
    post :proceed, on: :member
  end

end
