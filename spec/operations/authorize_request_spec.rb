RSpec.describe AuthorizeRequest do

  context 'valid token' do
    let(:token) { 'token' }
    let(:user) { create(:user) }

    before do
      allow(JWTService).to receive(:decode)
        .with(token)
        .and_return('user_id' => user.id)
    end

    it 'returns the user' do
      operation = described_class.new(token)

      expect(operation.call).to eq(user)
    end
  end

  context 'missing user' do
    let(:token) { 'token' }

    before do
      allow(JWTService).to receive(:decode)
        .with(token)
        .and_return('user_id' => -1)
    end

    it 'raises an error' do
      operation = described_class.new(token)

      expect { operation.call }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  context 'invalid token' do
    it 'raises an error' do
      operation = described_class.new('invalid token')

      expect { operation.call }.to raise_error(AuthorizeRequest::InvalidTokenError, /invalid authentication token/)
    end
  end

end
