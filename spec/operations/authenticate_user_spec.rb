RSpec.describe AuthenticateUser do

  let(:user) { create(:user) }
  let(:email) { user.email }
  let(:password) { user.password }
  let(:token) { 'token' }

  before do
    allow(JWTService).to receive(:encode)
      .with(user_id: user.id)
      .and_return(token)
  end

  context 'valid email and password' do
    it 'returns token' do
      operation = described_class.new(email, password)

      expect(operation.call).to eq(token)
    end
  end

  context 'invalid password' do
    it 'raises an error' do
      operation = described_class.new(email, 'youshallnotpass')

      expect { operation.call }.to raise_error(AuthenticateUser::InvalidCredentials, /invalid authentication credentials/)
    end
  end

  context 'missing email' do
    it 'raises an error' do
      operation = described_class.new('unknown@example.com', password)

      expect { operation.call }.to raise_error(AuthenticateUser::InvalidCredentials, /invalid authentication credentials/)
    end
  end

end
