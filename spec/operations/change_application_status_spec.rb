RSpec.describe ChangeApplicationStatus do

  let(:application) { create(:application) }

  context 'desired status is allowed' do
    let(:desired_status) { 'read' }

    it 'transitions the application into the desired status' do
      # We can extract this part into a `subject`,
      # but prefer to keep the subject of the test
      # as close as possible to the example context.
      operation = described_class.new(application, desired_status)

      expect {
        operation.call
      }.to change {
        application.reload.status
      }.from('unread').to(desired_status)
    end
  end

  context 'desired status is not allowed' do
    let(:desired_status) { 'accepted' }

    it 'raises an error without application status changes' do
      operation = described_class.new(application, desired_status)

      expect { operation.call }.to raise_error(
        ChangeApplicationStatus::IncorrectStatusError,
        /incorrect status 'accepted' during the transition from 'unread'/
      )
      expect(application.reload.status).to eq('unread')
    end
  end

end
