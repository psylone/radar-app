RSpec.describe 'Authentication API', type: :request do

  describe 'POST /auth' do
    let(:user) { create(:user) }
    let(:email) { user.email }
    let(:password) { user.password }
    let(:token) { 'token' }

    context 'with valid credentials' do
      let(:auth_operation) { double('authenticate user operation', call: token) }

      before do
        allow(AuthenticateUser).to receive(:new)
          .with(email, password)
          .and_return(auth_operation)
      end

      it 'returns token' do
        post '/auth', params: { email: email, password: password }

        expect(response).to have_http_status(:created)
        expect(json_response['token']).to eq(token)
      end
    end

    context 'with invalid password' do
      let(:error) { { 'error' => 'invalid authentication credentials', 'status' => 401 } }

      it 'returns unauthorized with error' do
        post '/auth', params: { email: email, password: 'youshallnotpass' }

        expect(response).to have_http_status(:unauthorized)
        expect(json_response).to a_hash_including(error)
      end
    end

    context 'with missing email' do
      let(:error) { { 'error' => 'invalid authentication credentials', 'status' => 401 } }

      it 'returns unauthorized with error' do
        post '/auth', params: { email: 'unknown@example.com', password: password }

        expect(response).to have_http_status(:unauthorized)
        expect(json_response).to a_hash_including(error)
      end
    end

    context 'with missing parameters' do
      let(:error) { { 'error' => 'param is missing or the value is empty: password', 'status' => 400 } }

      it 'returns unprocessable entity with error' do
        post '/auth', params: { email: email }

        expect(response).to have_http_status(:bad_request)
        expect(json_response).to a_hash_including(error)
      end
    end
  end

end
