RSpec.describe 'Applications API', type: :request do

  describe 'POST /applications/:application_id/proceed' do
    let(:application) { create(:application) }
    let(:token) { AuthenticateUser.new(application.recruiter.email, application.recruiter.password).call }
    let(:headers) { { 'Authorization' => "Bearer #{token}" } }

    context 'when the status is correct' do
      let(:desired_status) { 'read' }

      # Since we have no any render layer atm, let's define JSON response explicitly.
      # It can be replaced with JSON Schema or some kind of JSON validator.
      let(:application_json_response) do
        {
          id: application.id,
          status: desired_status,
          recruiter_id: application.recruiter_id,
          candidate_id: application.candidate_id,
          created_at: application.created_at.iso8601,
          updated_at: application.updated_at.iso8601
        }.stringify_keys
      end

      it 'changes application status' do
        post "/applications/#{application.id}/proceed", params: { status: desired_status }, headers: headers

        expect(response).to have_http_status(:created)
        expect(application.reload).to be_read
      end

      it 'returns the application in JSON format' do
        post "/applications/#{application.id}/proceed", params: { status: desired_status }, headers: headers

        expect(json_response).to eq(application_json_response)
      end
    end

    context 'when the status is incorrect' do
      let(:desired_status) { 'unknown' }
      let(:error) { { 'error' => %{incorrect status 'unknown' during the transition from 'unread'}, 'status' => 422 } }

      it 'returns unprocessable entity with error' do
        post "/applications/#{application.id}/proceed", params: { status: desired_status }, headers: headers

        expect(response).to have_http_status(:unprocessable_entity)
        expect(json_response).to a_hash_including(error)
      end
    end

    context 'when the status is missing' do
      let(:error) { { 'error' => 'param is missing or the value is empty: status', 'status' => 400 } }

      it 'returns unprocessable entity with error' do
        post "/applications/#{application.id}/proceed", headers: headers

        expect(response).to have_http_status(:bad_request)
        expect(json_response).to a_hash_including(error)
      end
    end

    context 'when the application is missing' do
      let(:missing_application_id) { -1 }
      let(:error) { { 'error' => %{Couldn't find Application with 'id'=#{missing_application_id}}, 'status' => 404 } }

      it 'returns not found with error' do
        post "/applications/#{missing_application_id}/proceed", headers: headers

        expect(response).to have_http_status(:not_found)
        expect(json_response).to a_hash_including(error)
      end
    end

    context 'when the user is not a recruiter' do
      let(:token) { AuthenticateUser.new(application.candidate.email, application.candidate.password).call }
      let(:error) { { 'error' => 'forbidden', 'status' => 403 } }

      it 'returns forbidden with error' do
        post "/applications/#{application.id}/proceed", headers: headers

        expect(response).to have_http_status(:forbidden)
        expect(json_response).to a_hash_including(error)
      end
    end
  end

end
