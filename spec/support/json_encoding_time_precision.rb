# By default ActiveSupport serializes time objects into the iso8601 format
# with a millisecond precision (3 digits after the fractional point).
# It can create some problems during the comparison of the JSON responses.
# Thus for the test environment let's change the time precision to zero.
ActiveSupport::JSON::Encoding.time_precision = 0
