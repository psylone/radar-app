RSpec.describe ApplicationController, type: :controller do

  describe 'request authentication' do
    controller do
      def index
      end
    end

    before do
      # The big intention is to pass headers through the request method like:
      #   get :index, headers: headers
      # But it didn't work: https://github.com/rspec/rspec-rails/issues/1655
      request.headers['Authorization'] = authorization_header
    end

    context 'valid authentication token' do
      let(:user) { build(:user) }
      let(:token) { 'token' }
      let(:authorization_header) { "Bearer #{token}" }
      let(:auth_request_operation) { double('authenticate request operation', call: user) }

      before do
        allow(AuthorizeRequest).to receive(:new)
          .with(token)
          .and_return(auth_request_operation)
      end

      it 'performs the endpoint with a proper user' do
        get :index

        expect(response).to have_http_status(:no_content)
        expect(controller.current_user).to eq(user)
      end
    end

    context 'invalid authentication token' do
      let(:authorization_header) { 'Bearer token' }
      let(:error) { { 'error' => 'invalid authentication token', 'status' => 401 } }

      it 'returns unauthorized with error' do
        get :index

        expect(response).to have_http_status(:unauthorized)
        expect(json_response).to a_hash_including(error)
      end
    end

    context 'missing authentication token' do
      let(:authorization_header) { nil }
      let(:error) { { 'error' => 'invalid authentication token', 'status' => 401 } }

      it 'returns unauthorized with error' do
        get :index

        expect(response).to have_http_status(:unauthorized)
        expect(json_response).to a_hash_including(error)
      end
    end
  end

end
