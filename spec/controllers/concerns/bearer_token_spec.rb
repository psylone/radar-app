RSpec.describe BearerToken, type: :controller do

  # Since BearerToken is a controller concern,
  # let's test it inside the controller context.
  controller(ApplicationController) do
    include BearerToken
  end

  subject { controller.bearer_token }

  before { allow(controller.request).to receive(:headers).and_return(headers) }

  context 'header has a valid type and a token' do
    let(:token) { 'token' }
    let(:headers) { { 'Authorization' => "Bearer #{token}" } }

    it 'returns token' do
      expect(subject).to eq(token)
    end
  end

  context 'header type is missing' do
    let(:headers) { { 'Authorization' => 'token' } }

    it 'returns nil' do
      expect(subject).to be_nil
    end
  end

  context 'header has a different type' do
    let(:headers) { { 'Authorization' => 'Basic token' } }

    it 'returns nil' do
      expect(subject).to be_nil
    end
  end

  context 'token is missing' do
    let(:headers) { { 'Authorization' => 'Bearer' } }

    it 'returns nil' do
      expect(subject).to be_nil
    end
  end

  context 'header is missing' do
    let(:headers) { {} }

    it 'returns nil' do
      expect(subject).to be_nil
    end
  end

end
