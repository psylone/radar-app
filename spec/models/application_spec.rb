RSpec.describe Application, type: :model do

  describe 'validations' do
    it { should validate_presence_of(:status) }
  end

  describe 'associations' do
    it { should belong_to(:recruiter) }
    it { should belong_to(:candidate) }
  end

  describe 'statuses' do
    it { should have_states(:unread, :read, :rejected, :declined, :improper, :accepted) }
    it { should handle_events(:read, :reject, when: :unread) }
    it { should handle_events(:decline, :improper, :accept, when: :read) }
  end

end
