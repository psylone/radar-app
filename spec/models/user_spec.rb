RSpec.describe User, type: :model do

  describe 'validations' do
    subject { build(:candidate) }

    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:email) }
    it { should validate_uniqueness_of(:email) }
    it { should validate_presence_of(:password_digest) }
    it { should validate_presence_of(:kind) }
    it { should validate_inclusion_of(:kind).in_array(%w[recruiter candidate]) }
  end

  describe 'associations' do
    it { should have_many(:submitted_applications).dependent(:destroy) }
    it { should have_many(:responded_applications).dependent(:destroy) }
  end

end
