RSpec.describe JWTService do

  subject { described_class }

  describe '.encode' do
    let(:payload) { { user_id: 101 } }
    let(:secret) { 'secret' }
    let(:token) { 'token' }

    before do
      stub_const('JWTService::HMAC_SECRET', secret)
      allow(JWT).to receive(:encode)
        .with(payload, secret)
        .and_return(token)
    end

    it 'encodes payload' do
      expect(subject.encode(payload)).to eq(token)
    end
  end

  describe '.decode' do
    context 'valid token' do
      let(:secret) { 'secret' }
      let(:user_id) { 101 }

      before { stub_const('JWTService::HMAC_SECRET', secret) }

      it 'decodes token' do
        token = JWT.encode({ user_id: user_id }, secret)

        expect(subject.decode(token)).to a_hash_including('user_id' => user_id)
      end
    end

    context 'invalid token' do
      it 'raises an error' do
        token = 'invalid token'

        expect { subject.decode(token) }.to raise_error(JWT::DecodeError)
      end
    end

    context 'invalid hmac secret' do
      let(:secret) { 'secret' }
      let(:user_id) { 101 }

      it 'raises an error' do
        token = JWT.encode({ user_id: user_id }, secret)

        expect { subject.decode(token) }.to raise_error(JWT::VerificationError)
      end
    end
  end

end
