FactoryBot.define do
  factory :user, aliases: [:candidate] do
    name { Faker::Name.name }
    email { Faker::Internet.email }
    password 'givemeatoken'
  end

  factory :recruiter, parent: :user do
    kind 'recruiter'
  end
end
