FactoryBot.define do
  factory :application do
    status 'unread'
    candidate { build(:candidate) }
    recruiter { build(:recruiter) }
  end
end
